package pl.sda.painter;

public class ShapePainter {

    public static void main(String[] args) {
        paintTriangle(10);
    }

    public static void paintTriangle(int h) {
        for (int y = 1; y < h + 1; y++) {
            for (int x = 0; x < y; x++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    public static void paintSquare(int a) {
        paintRect(a, a);
    }

    public static void paintRect(int w, int h) {
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
