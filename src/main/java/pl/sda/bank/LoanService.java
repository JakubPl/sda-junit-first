package pl.sda.bank;

import pl.sda.bank.exception.LoanNotApproved;
import pl.sda.bank.model.Customer;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDate;

public class LoanService {
    private BigDecimal funds;
    private Clock clock;

    public LoanService(BigDecimal funds, Clock clock) {
        this.funds = funds;
        this.clock = clock;
    }

    public BigDecimal calculateCreditworthiness(Customer customer) {
        validatePreconditions(customer);
        BigDecimal maxLoan = customer.getBalance().multiply(BigDecimal.TEN);
        int yearOfBirth = customer.getBirthdate().getYear();
        int currentYear = LocalDate.now(clock).getYear();
        if (currentYear - yearOfBirth < 20) {
            maxLoan = maxLoan.divide(BigDecimal.valueOf(2L));
        }
        maxLoan = maxLoan.subtract(customer.getLoanSum());
        if (maxLoan.compareTo(BigDecimal.ZERO) < 0) {
            maxLoan = BigDecimal.ZERO;
        }
        return maxLoan;
    }

    public void getLoan(Customer customer, BigDecimal requiredSum) {
        BigDecimal creditworthiness = calculateCreditworthiness(customer);
        if (creditworthiness.compareTo(requiredSum) >= 0 && this.funds.compareTo(BigDecimal.ZERO) > 0) {
            this.funds = this.funds.subtract(requiredSum);
        } else {
            throw new LoanNotApproved();
        }
    }

    public BigDecimal getFunds() {
        return funds;
    }

    private void validatePreconditions(Customer customer) {
        if (customer.isBlacklisted() || customer.getBirthdate().isAfter(LocalDate.now(clock))) {
            throw new IllegalArgumentException("Call security! " + customer.getPesel());
        }
    }
}
