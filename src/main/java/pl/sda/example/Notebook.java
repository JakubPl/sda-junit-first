package pl.sda.example;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Notebook {
    private String name;
    private BigDecimal price;
    private LocalDate bought;
    private int amount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public LocalDate getBought() {
        return bought;
    }

    public void setBought(LocalDate bought) {
        this.bought = bought;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Notebook{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", bought=" + bought +
                ", amount=" + amount +
                '}';
    }
}
