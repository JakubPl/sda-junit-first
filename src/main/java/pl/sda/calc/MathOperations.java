package pl.sda.calc;

import java.math.BigDecimal;
import java.util.Scanner;

public final class MathOperations {


    private final BigDecimal a;
    private final BigDecimal b;

    public MathOperations(BigDecimal a, BigDecimal b) {
        this.a = a;
        this.b = b;
    }

    public BigDecimal sum() {
        return a.add(b);
    }

    public BigDecimal max() {
        return a.max(b);
    }

    public BigDecimal min() {
        return a.min(b);
    }

    public void throwWhenEqual() {
        if (a.compareTo(b) == 0) {
            throw new RuntimeException("They are equal");
        }
    }

    public BigDecimal subtractResultHaveToBeGreaterThanZero() {
        BigDecimal operationResult = a.subtract(b);
        if (operationResult.compareTo(BigDecimal.ZERO) <= 0) {
            throw new RuntimeException("Subtraction result should be greater than zero");
        }
        return operationResult;
    }

    public BigDecimal multiply() {
        return a.multiply(b);
    }

    public BigDecimal subtract() {
        return a.subtract(b);
    }

    public BigDecimal divide() {
        if (b.compareTo(BigDecimal.ZERO) == 0) {
            throw new RuntimeException("b was lower than zero");
        }
        return a.divide(b);
    }

    public BigDecimal power() {
        BigDecimal tmp = a;
        for (BigDecimal i = BigDecimal.ONE; i.compareTo(b) < 0;
             i = i.add(BigDecimal.ONE)) {
            tmp = tmp.multiply(a);
        }
        return tmp;
    }

    public BigDecimal mod() {
        return a.remainder(b);
    }
}
