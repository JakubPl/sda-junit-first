package pl.sda.weather;

import static java.time.DayOfWeek.FRIDAY;
import static java.time.DayOfWeek.MONDAY;
import static java.time.DayOfWeek.SUNDAY;
import pl.sda.weather.model.Location;
import pl.sda.weather.model.WeatherType;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class AirfieldControl {
    public static final String RAIN_MONDAY_MSG = "Another rainy monday, start normally";
    public static final String DEFAULT_RESPONSE_MSG = "Buissness as usual, get going";
    public static final String PARTY_TIME_MSH = "Party time!";
    public static final String SUN_SUNDAY_MSG = "Head back to hangar, it's too nice today";
    public static final String SNOW_MSG = "Wait for assistance, visibility low";

    private WeatherDownloader weatherDownloader;
    private Location airfieldLocation;
    public AirfieldControl(WeatherDownloader weatherDownloader, Location airfieldLocation) {
        this.weatherDownloader = weatherDownloader;
        this.airfieldLocation = airfieldLocation;
    }

    public String getActionPreStartCommunication() {
        WeatherType weather = weatherDownloader.getWeather(airfieldLocation);
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        if (weather.equals(WeatherType.RAIN) && dayOfWeek.equals(MONDAY)) {
            return RAIN_MONDAY_MSG;
        } else if (weather.equals(WeatherType.SNOW)) {
            return SNOW_MSG;
        } else if (weather.equals(WeatherType.SUN) && dayOfWeek.equals(SUNDAY)) {
            return SUN_SUNDAY_MSG;
        } else if (dayOfWeek.equals(FRIDAY)) {
            return PARTY_TIME_MSH;
        } else {
            return DEFAULT_RESPONSE_MSG;
        }
    }
}
