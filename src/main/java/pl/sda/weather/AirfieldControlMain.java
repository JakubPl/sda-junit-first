package pl.sda.weather;

import okhttp3.OkHttpClient;
import pl.sda.weather.model.Location;

public class AirfieldControlMain {
    public static void main(String[] args) {
        AirfieldControl polandControl = new AirfieldControl(new WeatherDownloader(new OkHttpClient()), Location.POLAND);
        AirfieldControl gbControl = new AirfieldControl(new WeatherDownloader(new OkHttpClient()), Location.GREAT_BRITAN);
        System.out.println(gbControl.getActionPreStartCommunication());
    }
}
