package pl.sda.weather;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import pl.sda.weather.exception.WeatherServiceDownException;
import pl.sda.weather.model.Location;
import pl.sda.weather.model.WeatherType;

public class WeatherDownloader {
    private OkHttpClient client;

    public WeatherDownloader(OkHttpClient client) {
        this.client = client;
    }

    public WeatherType getWeather(Location location) {
        String downloadedWeather = downloadFile("https://www.metaweather.com/api/location/" + location.getId());
        int rainOccurrences = StringUtils.countMatches(downloadedWeather, WeatherType.RAIN.getWeatherString());
        int snowOccurrences = StringUtils.countMatches(downloadedWeather, WeatherType.SNOW.getWeatherString());
        int sunOccurrences = StringUtils.countMatches(downloadedWeather, WeatherType.SUN.getWeatherString());
        if (rainOccurrences > snowOccurrences && rainOccurrences > sunOccurrences) {
            return WeatherType.RAIN;
        } else if (sunOccurrences > snowOccurrences && sunOccurrences > rainOccurrences) {
            return WeatherType.SUN;
        } else {
            return WeatherType.SNOW;
        }
    }

    private String downloadFile(String url) {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (Exception e) {
            throw new WeatherServiceDownException();
        } finally {
            response.close();
        }
    }
}
