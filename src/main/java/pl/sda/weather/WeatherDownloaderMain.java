package pl.sda.weather;

import okhttp3.OkHttpClient;
import pl.sda.weather.model.Location;

public class WeatherDownloaderMain {
    public static void main(String[] args) {
        WeatherDownloader downloader = new WeatherDownloader(new OkHttpClient());
        System.out.println(downloader.getWeather(Location.POLAND));
    }
}
