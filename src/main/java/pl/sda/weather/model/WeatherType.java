package pl.sda.weather.model;

public enum WeatherType {
    SUN("Clear"), RAIN("Showers"), SNOW("Snow");

    private final String weatherString;

    WeatherType(String clear) {
        this.weatherString = clear;
    }

    public String getWeatherString() {
        return weatherString;
    }
}
