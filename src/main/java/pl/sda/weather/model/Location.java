package pl.sda.weather.model;

public enum Location {
    POLAND("523920"), GREAT_BRITAN("44418");

    private final String id;

    Location(String id) {

        this.id = id;
    }

    public String getId() {
        return id;
    }
}
