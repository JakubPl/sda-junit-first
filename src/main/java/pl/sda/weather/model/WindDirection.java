package pl.sda.weather.model;

public enum WindDirection {
    N,W,S,E
}
