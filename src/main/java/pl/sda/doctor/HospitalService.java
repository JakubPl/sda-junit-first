package pl.sda.doctor;

import pl.sda.doctor.model.Patient;

import java.util.HashMap;
import java.util.Map;

public class HospitalService {
    private PatientService patientService;
    private Map<String, Integer> patientAmountPerDoctor = new HashMap<>();

    public HospitalService(PatientService patientService) {
        this.patientService = patientService;
    }

    public void add(Patient patient) {
        patientService.addPatient(patient);
    }

    public void handleNextPatient() {
        String doctor = patientService.nextPatient();
        int currentPatientCountForDoctor;
        if(!patientAmountPerDoctor.containsKey(doctor)) {
            currentPatientCountForDoctor = 0;
        } else {
            currentPatientCountForDoctor = patientAmountPerDoctor.get(doctor);
        }
        currentPatientCountForDoctor ++;
        patientAmountPerDoctor.put(doctor, currentPatientCountForDoctor);
    }

    public Map<String, Integer> getPatientAmount() {
        return patientAmountPerDoctor;
    }
}
