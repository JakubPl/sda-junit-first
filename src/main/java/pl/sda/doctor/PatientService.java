package pl.sda.doctor;

import pl.sda.doctor.model.Condition;
import pl.sda.doctor.model.Patient;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.stream.Collectors;

public class PatientService {
    public static final String GOOD_CONDITION_DOCTOR = "dr. Gates";
    public static final String BAD_CONDITION_DOCTOR = "dr. Jobs";
    public static final String VERY_BAD_CONDITION_DOCTOR = "dr. Wozniak";
    private static final int MAX_QUEUE_LENGTH = 5;
    private Queue<Patient> patientQueue = new LinkedList<>();

    public boolean addPatient(Patient patient) {

        if (patientQueue.size() >= MAX_QUEUE_LENGTH) {
            return false;
        }
        patientQueue.add(patient);
        return true;
    }

    /**
     * Purges patient queue and returns all patients in format name + " " + surname that where in the queue
     *
     * @return patients that were in queue
     */
    public List<String> removeAllRemaining() {
        List<String> allNamesAndSurnames = patientQueue.stream()
                .map(patient -> String.format("%s %s", patient.getName(), patient.getSurname()))
                .collect(Collectors.toList());
        patientQueue.clear();
        return allNamesAndSurnames;
    }

    public String nextPatient() {
        Patient nextPatient = patientQueue.remove();
        final Condition condition = nextPatient.getCondition();
        if (condition == null || Condition.GOOD.equals(condition)) {
            return GOOD_CONDITION_DOCTOR;
        } else if (Condition.BAD.equals(condition)) {
            return BAD_CONDITION_DOCTOR;
        } else {
            return VERY_BAD_CONDITION_DOCTOR;
        }
    }
}
