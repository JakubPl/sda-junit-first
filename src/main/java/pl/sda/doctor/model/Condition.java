package pl.sda.doctor.model;

public enum  Condition {
    GOOD, BAD, VERY_BAD;
}
