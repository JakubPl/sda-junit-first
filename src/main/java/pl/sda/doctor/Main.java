package pl.sda.doctor;

import pl.sda.doctor.model.Condition;
import pl.sda.doctor.model.Patient;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        PatientService patientService = new PatientService();
        Scanner scanner = new Scanner(System.in);
        String nextString = null;
        while (!"quit".equals(nextString)) {
            printMenu();
            nextString = scanner.nextLine();
            if (nextString.equals("add")) {
                Patient patient = buildPatient(scanner);
                boolean isPatientAdded = patientService.addPatient(patient);
                if(isPatientAdded){
                    System.out.println("Added " + patient.getName() + " " + patient.getSurname());
                } else {
                    System.out.println("Sorry, queue is too long :(");
                }
            } else if (nextString.equals("next")) {
                System.out.println(patientService.nextPatient());
            }
        }
        System.out.println("These patients did not receive help today: ");
        patientService.removeAllRemaining().forEach(System.out::println);
    }

    private static void printMenu() {
        System.out.println("Possible options:\n\tadd\n\tnext\n\tquit");
    }

    private static Patient buildPatient(Scanner scanner) {
        System.out.println("Name:");
        String patientName = scanner.nextLine();
        System.out.println("Surname:");
        String patientSurname = scanner.nextLine();
        System.out.println("Condition:");
        Condition patientCondition = Condition.valueOf(scanner.nextLine());
        Patient patient = new Patient();
        patient.setName(patientName);
        patient.setSurname(patientSurname);
        patient.setCondition(patientCondition);
        return patient;
    }
}
