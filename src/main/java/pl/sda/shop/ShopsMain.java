package pl.sda.shop;

import pl.sda.shop.impl.DefaultAvailableShopsServiceImpl;
import pl.sda.shop.model.Shop;

import java.time.Clock;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ShopsMain {

    private static final List<Shop> SHOPS = Arrays.asList(
            new Shop("Media markt", 8, 20),
            new Shop("Media expert", 16, 18),
            new Shop("H&M", 8, 22)
    );

    public static void main(String[] args) {
        AvailableShopsService service = new DefaultAvailableShopsServiceImpl(SHOPS, Clock.systemDefaultZone());
        System.out.println("Podaj fragment nazwy sklepu, a ja sprawdze czy jest otwarty.");
        Scanner scanner = new Scanner(System.in);
        String shopName = scanner.nextLine();
        service.findOpenShopsWithNameContaining(shopName)
                .stream()
                .map(Shop::getName)
                .forEach(System.out::println);
    }
}
