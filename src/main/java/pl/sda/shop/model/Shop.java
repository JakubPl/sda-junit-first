package pl.sda.shop.model;

public class Shop {
    private final String name;
    private final int openFromHour;
    private final int openToHour;

    public Shop(String name, int openFrom, int openTo) {
        this.name = name;
        this.openFromHour = openFrom;
        this.openToHour = openTo;
    }

    public String getName() {
        return name;
    }

    public int getOpenFromHour() {
        return openFromHour;
    }

    public int getOpenToHour() {
        return openToHour;
    }
}
