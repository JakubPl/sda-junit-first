package pl.sda.shop.impl;

import pl.sda.shop.AvailableShopsService;
import pl.sda.shop.model.Shop;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultAvailableShopsServiceImpl implements AvailableShopsService {
    private final List<Shop> shops;
    private final Clock clock;

    public DefaultAvailableShopsServiceImpl(List<Shop> shops, Clock clock) {
        this.shops = shops;
        this.clock = clock;
    }

    @Override
    public List<Shop> findOpenShopsWithNameContaining(String name) {


        return shops.stream()
                .filter(shop -> shop.getName().contains(name) && isShopOpen(shop))
                .collect(Collectors.toList());
    }

    private boolean isShopOpen(Shop shop) {
        final LocalTime currentLocalTime = LocalTime.now(clock);
        return (LocalTime.of(shop.getOpenFromHour(), 0).isBefore(currentLocalTime)
                || LocalTime.of(shop.getOpenFromHour(), 0).equals(currentLocalTime))
                && (LocalTime.of(shop.getOpenToHour(), 0).isAfter(currentLocalTime)
                || LocalTime.of(shop.getOpenToHour(), 0).equals(currentLocalTime));
    }

    @Override
    public List<Shop> findOpenShops() {
        final LocalDateTime currentDateTime = LocalDateTime.now(clock);
        final LocalTime currentLocalTime = currentDateTime.toLocalTime();

        return shops.stream()
                .filter(shop -> isShopOpen(shop))
                .collect(Collectors.toList());
    }
}
