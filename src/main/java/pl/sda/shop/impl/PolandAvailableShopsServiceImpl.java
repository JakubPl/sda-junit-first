package pl.sda.shop.impl;

import pl.sda.shop.model.Shop;

import java.time.Clock;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class PolandAvailableShopsServiceImpl extends DefaultAvailableShopsServiceImpl {
    private final Clock clock;

    public PolandAvailableShopsServiceImpl(List<Shop> shops, Clock clock) {
        super(shops, clock);
        this.clock = clock;
    }

    @Override
    public List<Shop> findOpenShopsWithNameContaining(String name) {
        LocalDate now = LocalDate.now(clock);
        DayOfWeek dayOfWeek = now.getDayOfWeek();
        if(dayOfWeek.equals(DayOfWeek.SUNDAY)) {
            return Collections.emptyList();
        }
        return super.findOpenShopsWithNameContaining(name);
    }

    @Override
    public List<Shop> findOpenShops() {
        LocalDate now = LocalDate.now(clock);
        DayOfWeek dayOfWeek = now.getDayOfWeek();
        if(dayOfWeek.equals(DayOfWeek.SUNDAY)) {
            return Collections.emptyList();
        }
        return super.findOpenShops();
    }
}
