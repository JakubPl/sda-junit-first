package pl.sda.shop;

import pl.sda.shop.model.Shop;

import java.util.List;

public interface AvailableShopsService {
    List<Shop> findOpenShopsWithNameContaining(String name);
    List<Shop> findOpenShops();
}
