package pl.sda.db;

import pl.sda.db.exception.UserNotFoundException;
import pl.sda.db.model.User;

import java.time.Clock;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public class UserDatabase {
    private List<User> users = new LinkedList<>();
    private Clock clock;

    public UserDatabase(Clock clock) {
        this.clock = clock;
    }

    public void registerUser(User user) {
        Objects.requireNonNull(user);
        if (user.getName().length() < 3) {
            throw new IllegalArgumentException("User name too short");
        } else if (user.getBirthDate().isBefore(LocalDate.now(clock))) {
            throw new IllegalArgumentException("Invalid birth date");
        } else if (user.getPassword().length() < 7) {
            throw new IllegalArgumentException("Invalid birth date");
        } else if (user.getPesel().length() != 11) {
            throw new IllegalArgumentException("Invalid PESEL");
        } else if (findUserWithPesel(user.getPesel()).isPresent()) {
            throw new IllegalStateException("User with such PESEL already exists");
        }
        users.add(user);
    }

    public List<User> findUsersByNameContaining(String name) {
        return users.stream()
                .filter(e -> e.getName().contains(name))
                .collect(Collectors.toList());
    }

    public List<User> findUsersWithPersonalPage() {
        return users.stream()
                .filter(e -> !e.getPersonalSite().isEmpty())
                .collect(Collectors.toList());
    }

    public List<User> findUsersWithInvalidPeselAndBirthDate() {
        return users.stream()
                .filter(e -> !e.getPersonalSite().isEmpty())
                .collect(Collectors.toList());
    }

    public Optional<User> findUserWithPesel(String pesel) {
        return users.stream()
                .filter(e -> e.getPesel().equals(pesel))
                .findFirst();
    }

    public List<User> findUsersWithBirthDateBetween(LocalDate from, LocalDate to) {
        return users.stream()
                .filter(e -> (e.getBirthDate().isAfter(from) && e.getBirthDate().isBefore(to)) || e.getBirthDate().isEqual(to) || e.getBirthDate().isEqual(from))
                .collect(Collectors.toList());
    }

    public User login(String name, String password) {
        return users.stream()
                .filter(e -> e.getName().equals(name) && e.getPassword().equals(password))
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    public List<User> findAll() {
        return users;
    }
}
