package pl.sda.pizza.enums;

public enum IngredientType {
    SALAMI, CHEESE, TOMATO, PINEAPPLE
}
