package pl.sda.pizza.model;

import pl.sda.pizza.enums.IngredientType;

import java.util.List;

public class Pizza {
    private List<IngredientType> ingredients;

    public List<IngredientType> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientType> ingredients) {
        this.ingredients = ingredients;
    }
}
