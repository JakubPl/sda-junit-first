package pl.sda.pizza;

import pl.sda.pizza.enums.IngredientType;
import pl.sda.pizza.model.Pizza;

import java.util.LinkedList;
import java.util.List;

public class PizzaMaker {
    public Pizza acceptOrder(String messageFromClient) {
        Pizza pizza = new Pizza();
        List<IngredientType> ingredients = new LinkedList<>();
        parseMessageFromClient(messageFromClient, ingredients);
        pizza.setIngredients(ingredients);
        return pizza;
    }

    private void parseMessageFromClient(String messageFromClient, List<IngredientType> ingredients) {
        if (messageFromClient.contains("salami")) {
            ingredients.add(IngredientType.SALAMI);
        }
        if (messageFromClient.contains("pomidor")) {
            ingredients.add(IngredientType.TOMATO);
        }
        if (messageFromClient.contains("ser")) {
            ingredients.add(IngredientType.CHEESE);
        }
        if (messageFromClient.contains("ananas")) {
            ingredients.add(IngredientType.PINEAPPLE);
        }
    }
}
