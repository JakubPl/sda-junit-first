package pl.sda.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MockitoExampleTest {
    @Test
    public void mockTest() {
        Object mockedObject = mock(Object.class);
        when(mockedObject.toString()).thenReturn("abc");
        System.out.println(mockedObject.toString());
    }

    @Test
    public void mockTest2() {
        MojaUlubionaKlasa mojaUlubionaKlasaMock = mock(MojaUlubionaKlasa.class);
        when(mojaUlubionaKlasaMock.zrobCos(eq("ala ma kota"))).thenReturn("ala ma psa");
        System.out.println(mojaUlubionaKlasaMock.zrobCos("123"));//().isEqualTo("123");
    }

    @Test
    public void mockVerifyTest() {
        List<String> mockedList = mock(List.class);
        mockedList.get(0);
        verify(mockedList).get(eq(0));
    }
}
