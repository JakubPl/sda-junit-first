package pl.sda.example;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;
import java.time.LocalDate;

@RunWith(JUnitParamsRunner.class)
public class ParamsTest {

    //more here: https://github.com/Pragmatists/JUnitParams/blob/master/src/test/java/junitparams/usage/SamplesOfUsageTest.java
    @Test
    @FileParameters("src/test/resources/file-params-example.csv")
    public void givenFileShouldParseCorrectly(String name, BigDecimal price, String bough, int amount) {
        Notebook notebook = new Notebook();
        notebook.setName(name);
        notebook.setPrice(price);
        notebook.setBought(LocalDate.parse(bough));
        notebook.setAmount(amount);

        System.out.println(notebook.toString());
    }

    @Test
    @Parameters({"Sony, 2000, 2011-01-01, 20",
            "HP, 4400.22, 2000-02-01, 12"})
    public void givenAnnotationParametersShouldParseCorrectly(String name, BigDecimal price, String bough, int amount) {
        Notebook notebook = new Notebook();
        notebook.setName(name);
        notebook.setPrice(price);
        notebook.setBought(LocalDate.parse(bough));
        notebook.setAmount(amount);

        System.out.println(notebook.toString());
    }

    @Test
    @Parameters(method = "notebooks")
    public void givenMethodParametersShouldParseCorrectly(String name, BigDecimal price, LocalDate bough, int amount) {
        Notebook notebook = new Notebook();
        notebook.setName(name);
        notebook.setPrice(price);
        notebook.setBought(bough);
        notebook.setAmount(amount);

        System.out.println(notebook.toString());
    }

    public Object[][] notebooks() {
        return new Object[][]{
                new Object[]{"Sony", BigDecimal.valueOf(123L), LocalDate.now(), 9},
                new Object[]{"Samsung", BigDecimal.valueOf(1235L), LocalDate.now(), 1}
        };
    }
}