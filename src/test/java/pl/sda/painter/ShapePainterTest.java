package pl.sda.painter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.PrintStream;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ShapePainterTest {

    @Mock
    public PrintStream printStreamMock;

    @Test
    public void givenWAndHEq5WhenPaintRectThenShouldPaintRect() {
        System.setOut(printStreamMock);

        ShapePainter.paintRect(5, 5);
        verify(printStreamMock, times(25))
                .print(eq("*"));
        verify(printStreamMock, times(5))
                .println();
    }

    @Test
    public void givenAEq6WhenPaintSquareThenShouldPaintSquare() {
        System.setOut(printStreamMock);

        ShapePainter.paintSquare(6);
        verify(printStreamMock, times(36))
                .print(eq("*"));
        verify(printStreamMock, times(6))
                .println();
    }

    @Test
    public void givenHEq3WhenPaintTriangleThenShouldPaintTriangle() {
        System.setOut(printStreamMock);
        ShapePainter.paintTriangle(3);


        InOrder inOrder = inOrder(printStreamMock);
        inOrder.verify(printStreamMock, times(1))
                .print(eq("*"));
        inOrder.verify(printStreamMock, times(1))
                .println();
        inOrder.verify(printStreamMock, times(2))
                .print(eq("*"));
        inOrder.verify(printStreamMock, times(1))
                .println();
        inOrder.verify(printStreamMock, times(3))
                .print(eq("*"));
    }
}