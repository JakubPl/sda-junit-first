package pl.sda.calc;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import java.math.BigDecimal;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.Assert.assertEquals;

@RunWith(JUnitParamsRunner.class)
public class MathOperationsTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void givenAEq2BEq3WhenPowThenShouldBeEqual8() {
        //given
        BigDecimal a = BigDecimal.valueOf(10);
        BigDecimal b = BigDecimal.valueOf(3);
        MathOperations operations = new MathOperations(a, b);
        //when
        BigDecimal resultOfPower = operations.power();
        //then
        assertEquals(BigDecimal.valueOf(1000), resultOfPower);
    }

    @Test
    public void givenAEq2BEq3WhenModThenShouldBeEqual2() {
        //given
        BigDecimal a = BigDecimal.valueOf(2);
        BigDecimal b = BigDecimal.valueOf(3);
        MathOperations operations = new MathOperations(a, b);
        //when
        BigDecimal resultOfPower = operations.mod();
        //then
        assertEquals(BigDecimal.valueOf(2), resultOfPower);
    }

    public Object[][] maxDataSource() {
        return new Object[][]{
                new Object[]{BigDecimal.ONE, BigDecimal.valueOf(10), BigDecimal.valueOf(10)},
                new Object[]{BigDecimal.TEN, BigDecimal.ONE, BigDecimal.valueOf(10)},
        };
    }

    @Test
    //@Parameters(method = "maxDataSource")
    /*@Parameters({"8, 10, 10",
            "10,8,10",
    "10,10,10"})*/
    @FileParameters("src/test/resources/calculation-data.csv")
    public void testMax(BigDecimal a, BigDecimal b, BigDecimal expectedResult) {
        //given
        MathOperations operations = new MathOperations(a, b);
        //when
        BigDecimal result = operations.max();
        //then
        assertEquals(expectedResult, result);
    }

    @Test
    public void givenAEq5BEq3WhenMaxThen5ShouldBeMax() {
        //given
        BigDecimal a = BigDecimal.valueOf(5);
        BigDecimal b = BigDecimal.valueOf(3);
        MathOperations operations = new MathOperations(a, b);
        //when
        BigDecimal result = operations.max();
        //then
        assertEquals(BigDecimal.valueOf(5), result);
    }

    @Test
    public void givenAEq6BEq6WhenMaxThen6ShouldBeMax() {
        //given
        BigDecimal a = BigDecimal.valueOf(6);
        BigDecimal b = BigDecimal.valueOf(6);
        MathOperations operations = new MathOperations(a, b);
        //when
        BigDecimal result = operations.max();
        //then
        assertEquals(BigDecimal.valueOf(6), result);
    }

    @Parameters({"10,2,2", "9,9,9", "2,10,2"})
    @Test
    public void testMin(BigDecimal a, BigDecimal b, BigDecimal expectedResult) {
        //given
        MathOperations mathOperations = new MathOperations(a, b);
        //when
        BigDecimal resultOfMin = mathOperations.min();
        //then
        assertThat(resultOfMin).isEqualTo(expectedResult);
    }

    public BigDecimal[][] divideTestSource() {
        return new BigDecimal[][] {
                new BigDecimal[] {BigDecimal.valueOf(10), BigDecimal.valueOf(2), BigDecimal.valueOf(5)},
                new BigDecimal[] {BigDecimal.valueOf(10), BigDecimal.ZERO, null}
        };
    }

    @Parameters(method = "divideTestSource")
    @Test
    public void testDivide(BigDecimal a, BigDecimal b, BigDecimal expectedResult) {
        if(expectedResult == null) {
            expectedException.expect(RuntimeException.class);
        }
        //given
        MathOperations mathOperations = new MathOperations(a, b);
        //when
        BigDecimal resultOfMin = mathOperations.divide();
        //then
        assertThat(resultOfMin).isEqualTo(expectedResult);
    }

    @Test
    public void givenAEq7BEq7WhenMaxThenExceptionShouldBeThrown() {
        expectedException.expect(RuntimeException.class);
        //given
        BigDecimal a = BigDecimal.valueOf(7);
        BigDecimal b = BigDecimal.valueOf(7);
        MathOperations operations = new MathOperations(a, b);
        //when
        operations.throwWhenEqual();
    }

    @Test(expected = RuntimeException.class)
    public void givenAEq7BEq7WhenSubtractResultHaveToBeGreaterThanZeroThenExceptionShouldBeThrown() {
        //given
        BigDecimal a = BigDecimal.valueOf(7);
        BigDecimal b = BigDecimal.valueOf(7);
        MathOperations operations = new MathOperations(a, b);
        //when
        operations.subtractResultHaveToBeGreaterThanZero();
    }

    @Test
    public void givenAEq20BEq7WhenSubtractResultHaveToBeGreaterThanZeroThenExceptionShouldBeThrown() {
        //given
        BigDecimal a = BigDecimal.valueOf(20);
        BigDecimal b = BigDecimal.valueOf(7);
        MathOperations operations = new MathOperations(a, b);
        //when
        BigDecimal result = operations.subtractResultHaveToBeGreaterThanZero();
        assertEquals(result, BigDecimal.valueOf(13));
    }

    @Test
    public void multiply() {
    }

    @Test
    public void subtract() {
    }

    @Test
    public void givenAEq20BEq2WhenDivideThenResultShouldBe10() {
        //given
        BigDecimal a = BigDecimal.valueOf(20);
        BigDecimal b = BigDecimal.valueOf(2);
        MathOperations operations = new MathOperations(a, b);
        //when
        BigDecimal divideResult = operations.divide();
        //then
        assertEquals(BigDecimal.TEN, divideResult);
    }


    @Test
    public void givenAEq20BEq9WhenDivideThenResultShouldThrowIllegalArgumentException() {
        expectedException.expect(RuntimeException.class);
        //given
        BigDecimal a = BigDecimal.valueOf(20);
        BigDecimal b = BigDecimal.ZERO;
        MathOperations operations = new MathOperations(a, b);
        //when
        operations.divide();
    }

/*
    public static class ExpectIllegalExceptionMatcher implements Matcher<IllegalArgumentException> {

        @Override
        public boolean matches(Object o) {
            return o instanceof IllegalArgumentException
                    && ((IllegalArgumentException) o).getMessage()
                    .equals("b was lower than zero");
        }

        @Override
        public void describeMismatch(Object o, Description description) {

        }

        @Override
        public void _dont_implement_Matcher___instead_extend_BaseMatcher_() {

        }

        @Override
        public void describeTo(Description description) {

        }
    }*/
}