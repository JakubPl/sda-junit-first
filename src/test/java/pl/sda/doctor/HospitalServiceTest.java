package pl.sda.doctor;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import pl.sda.doctor.model.Condition;
import pl.sda.doctor.model.Patient;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.*;
import static pl.sda.doctor.PatientService.GOOD_CONDITION_DOCTOR;

@RunWith(MockitoJUnitRunner.class)
public class HospitalServiceTest {

    public static final String DR_JAKIS = "dr Jakis";
    public static final String DR_INNY = "dr inny";

    @Test
    public void test() {
        //given
        PatientService patientService = mock(PatientService.class);
        HospitalService hospitalService = new HospitalService(patientService);
        when(patientService.nextPatient()).thenReturn(DR_JAKIS, DR_INNY);
        //when
        hospitalService.handleNextPatient();
        hospitalService.handleNextPatient();
        hospitalService.handleNextPatient();
        hospitalService.handleNextPatient();
        //then
        assertThat(hospitalService.getPatientAmount().get(DR_JAKIS)).isEqualTo(1);
        assertThat(hospitalService.getPatientAmount().get(DR_INNY)).isEqualTo(3);
        verify(patientService, times(4)).nextPatient();
    }
}