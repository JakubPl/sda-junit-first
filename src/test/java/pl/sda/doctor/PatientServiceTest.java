package pl.sda.doctor;

import org.junit.Before;
import org.junit.Test;
import pl.sda.doctor.model.Condition;
import pl.sda.doctor.model.Patient;

import java.util.NoSuchElementException;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static pl.sda.doctor.PatientService.*;

public class PatientServiceTest {
    private PatientService patientService;

    @Before
    public void before() {
        //given
        patientService = new PatientService();
        Patient patient1 = new Patient();
        patient1.setCondition(Condition.BAD);
        patient1.setName("Jakub");
        patient1.setSurname("Nowak");
        Patient patient2 = new Patient();
        patient2.setCondition(Condition.BAD);
        patient2.setName("Paweł");
        patient2.setSurname("Nowak");
        Patient patient3 = new Patient();
        patient3.setCondition(Condition.BAD);
        patient3.setName("Joanna");
        patient3.setSurname("Nowak");
        Patient patient4 = new Patient();
        patient4.setCondition(Condition.BAD);
        patient4.setName("Joanna");
        patient4.setSurname("Nowak");
        Patient patient5 = new Patient();
        patient5.setCondition(Condition.BAD);
        patient5.setName("Joanna");
        patient5.setSurname("Nowak");
        patientService.addPatient(patient1);
        patientService.addPatient(patient2);
        patientService.addPatient(patient3);
        patientService.addPatient(patient4);
        patientService.addPatient(patient5);
    }

    @Test
    public void givenPatientIsInBadCondtionWhenNextPatientThenShouldGoToJobs() {
        //given
        PatientService patientService = new PatientService();
        Patient patient = new Patient();
        patient.setCondition(Condition.BAD);
        patientService.addPatient(patient);
        //when
        String doctor = patientService.nextPatient();
        //then
        assertThat(doctor).isEqualTo(BAD_CONDITION_DOCTOR);
    }

    @Test
    public void givenPatientIsInGoodCondtionWhenNextPatientThenShouldGoToGates() {
        //given
        PatientService patientService = new PatientService();
        Patient patient = new Patient();
        patient.setCondition(Condition.GOOD);
        patientService.addPatient(patient);
        //when
        String doctor = patientService.nextPatient();
        //then
        assertThat(doctor).isEqualTo(GOOD_CONDITION_DOCTOR);
    }

    @Test
    public void givenPatientIsInVeryBadCondtionWhenNextPatientThenShouldGoToWozniak() {
        //given
        PatientService patientService = new PatientService();
        Patient patient = new Patient();
        patient.setCondition(Condition.VERY_BAD);
        patientService.addPatient(patient);
        //when
        String doctor = patientService.nextPatient();
        //then
        assertThat(doctor).isEqualTo(VERY_BAD_CONDITION_DOCTOR);
    }

    @Test
    public void givenPatientServiceHas0PatientsWhenAddingNewPatientOperationShouldFail() {
        //given
        PatientService patientServiceWithZeroPatients = new PatientService();
        //when
        Patient patient = new Patient();
        patientServiceWithZeroPatients.addPatient(patient);
        //then
        patientServiceWithZeroPatients.nextPatient();
    }

    @Test/*(expected = NoSuchElementException.class)*/
    public void givenPatientServiceHas5PatientsWhenAddingNewPatientOperationShouldFail() {
        //when
        boolean addingResult = patientService.addPatient(new Patient());
        //then
        assertThat(addingResult).isFalse();
        //IntStream.range(0,5).forEach(_ -> patientService.nextPatient());
        /*for(int i=0; i < 5; i ++) {
            patientService.nextPatient();
        }
        patientService.nextPatient();*/
    }
}