package pl.sda.shop.impl;

import org.junit.jupiter.api.Test;
import pl.sda.shop.model.Shop;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PolandAvailableShopsServiceImplTest {
    private static final List<Shop> SHOP_LIST = Arrays.asList(
            new Shop("Media markt", 3, 12),
            new Shop("Media markt Katowice", 3, 12),
            new Shop("Media Warszawa", 3, 12),
            new Shop("PROSTO", 20, 21));

    @Test
    void given4ShopsAndDayOfWeekIsSundayWhenFindOpenShopsThenNoShopsShouldBeReturned() {
        //given
        Clock fixedClock = Clock.fixed(Instant.parse("2019-02-03T11:00:00Z"), ZoneOffset.UTC);
        PolandAvailableShopsServiceImpl service = new PolandAvailableShopsServiceImpl(SHOP_LIST, fixedClock);
        //when
        List<Shop> openShops = service.findOpenShops();
        //then
        assertThat(openShops).isEmpty();
    }

    @Test
    void given4ShopsAndDayOfWeekIsMondayWhenFindOpenShopsThenNoShopsShouldBeReturned() {
        //given
        Clock fixedClock = Clock.fixed(Instant.parse("2019-02-04T11:00:00Z"), ZoneOffset.UTC);
        PolandAvailableShopsServiceImpl service = new PolandAvailableShopsServiceImpl(SHOP_LIST, fixedClock);
        //when
        List<Shop> openShops = service.findOpenShops();
        //then
        assertThat(openShops).isNotEmpty();
    }
}