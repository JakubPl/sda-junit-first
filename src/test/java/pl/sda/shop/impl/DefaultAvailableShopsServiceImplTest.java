package pl.sda.shop.impl;

import org.junit.Test;
import pl.sda.shop.model.Shop;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultAvailableShopsServiceImplTest {

    private static final List<Shop> SHOP_LIST = Arrays.asList(
            new Shop("Media markt", 3, 12),
            new Shop("Media markt Katowice", 3, 12),
            new Shop("Media Warszawa", 3, 12),
            new Shop("PROSTO", 20, 21));

    @Test
    public void givenTime4AndShopOpenBetween3And12WhenfindOpenShopsShouldReturn3Shops() {
        //given
        Clock fixedClock = Clock.fixed(Instant.parse("2018-08-22T12:00:00Z"), ZoneOffset.UTC);
        DefaultAvailableShopsServiceImpl service = new DefaultAvailableShopsServiceImpl(SHOP_LIST, fixedClock);
        //when
        List<Shop> openShops = service.findOpenShopsWithNameContaining("Media");
        //then
        //assertEquals(1, openShops.size());
        assertThat(openShops).hasSize(3);
        assertThat(openShops).extracting(shop -> shop.getName())
                .contains("Media Warszawa", "Media markt Katowice", "Media markt");

        /*assertEquals(openShops.get(0).getName(), "Media markt");
        assertEquals(openShops.get(1).getName(), "Media markt Katowice");
        assertEquals(openShops.get(2).getName(), "Media Warszawa");*/
    }

    @Test
    public void givenTime12And20MinutesAndShopOpenBetween3And12WhenfindOpenShopsContainingMediaShouldReturnNoShops() {
        //given
        Clock fixedClock = Clock.fixed(Instant.parse("2018-08-22T12:20:00Z"), ZoneOffset.UTC);
        DefaultAvailableShopsServiceImpl service = new DefaultAvailableShopsServiceImpl(SHOP_LIST, fixedClock);
        //when
        List<Shop> openShops = service.findOpenShopsWithNameContaining("Media");
        //then
        assertThat(openShops).isEmpty();
    }

    @Test
    public void givenTime12And20MinutesAndShopOpenBetween3And12WhenfindOpenShopsShouldReturnNoShops() {
        //given
        Clock fixedClock = Clock.fixed(Instant.parse("2018-08-22T12:20:00Z"), ZoneOffset.UTC);
        DefaultAvailableShopsServiceImpl service = new DefaultAvailableShopsServiceImpl(SHOP_LIST, fixedClock);
        //when
        List<Shop> openShops = service.findOpenShops();
        //then
        assertThat(openShops).isEmpty();
    }

    @Test
    public void givenTime04And20MinutesAndShopOpenBetween3And12WhenfindOpenShopsShouldReturn3Shops() {
        //given
        Clock fixedClock = Clock.fixed(Instant.parse("2018-08-22T04:20:00Z"), ZoneOffset.UTC);
        DefaultAvailableShopsServiceImpl service = new DefaultAvailableShopsServiceImpl(SHOP_LIST, fixedClock);
        //when
        List<Shop> openShops = service.findOpenShops();
        //then
        assertThat(openShops).hasSize(3);
        assertThat(openShops).extracting(e -> e.getName())
                .contains("Media Warszawa", "Media markt Katowice", "Media markt");;
    }


    @Test
    public void givenTime1AndShopOpenBetween3And12WhenfindOpenShopsContainingMediaShouldReturnNoShops() {
        //given
        Clock fixedClock = Clock.fixed(Instant.parse("2018-08-22T01:00:00Z"), ZoneOffset.UTC);
        DefaultAvailableShopsServiceImpl service = new DefaultAvailableShopsServiceImpl(SHOP_LIST, fixedClock);
        //when
        List<Shop> openShops = service.findOpenShopsWithNameContaining("Media");
        //then
        assertThat(openShops).isEmpty();
    }

    @Test
    public void givenTime5AndShopOpenBetween3And12WhenFindOpenShopsContainingEmpikShouldReturnNoShops() {
        //given
        Clock fixedClock = Clock.fixed(Instant.parse("2018-08-22T05:00:00Z"), ZoneOffset.UTC);
        DefaultAvailableShopsServiceImpl service = new DefaultAvailableShopsServiceImpl(SHOP_LIST, fixedClock);
        //when
        List<Shop> openShops = service.findOpenShopsWithNameContaining("Empik");
        //then
        assertThat(openShops).isEmpty();
    }


}