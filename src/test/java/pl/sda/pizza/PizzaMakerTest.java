package pl.sda.pizza;

import org.junit.Test;
import pl.sda.pizza.enums.IngredientType;
import pl.sda.pizza.model.Pizza;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

//salami, ser, pomidor, ananas

public class PizzaMakerTest {
    @Test
    public void test() {
        //given
        String messageFromClient = "czesc, chce pizze z salami, serem, pomidorem";
        PizzaMaker maker = new PizzaMaker();
        //when
        Pizza pizza = maker.acceptOrder(messageFromClient);
        //then
        assertThat(pizza.getIngredients())
                .contains(IngredientType.SALAMI, IngredientType.CHEESE, IngredientType.TOMATO);
    }
}
